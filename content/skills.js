export const languages = [
    [
        {
            name: 'C++',
            color: 'blue',
            perc: '45',
        },
        {
            name: 'Java',
            color: 'red',
            perc: '60',
        },
        {
            name: 'PHP',
            color: 'purple',
            perc: '75',
        },
        {
            name: 'Python',
            color: 'yellow',
            perc: '70',
        },
    ],
    [
        {
            name: 'HTML',
            color: 'orange',
            perc: '95',
        },
        {
            name: 'CSS/Sass',
            color: 'pink',
            perc: '70',
        },
        {
            name: 'JavaScript',
            color: 'yellow',
            perc: '75',
        },
    ],
];

export const libraries = [
    [
        {
            name: 'jQuery',
            color: 'blue',
            perc: '75',
        },
        {
            name: 'Express.js',
            color: 'messenger',
            perc: '55',
        },
        {
            name: 'Socket.io',
            color: 'gray',
            perc: '60',
        },
        {
            name: 'React.js',
            color: 'twitter',
            perc: '70',
        },
        {
            name: 'Next.js',
            color: 'gray',
            perc: '70',
        },
        {
            name: 'Flask',
            color: 'gray',
            perc: '45',
        },
    ],
    [
        {
            name: 'Bulma',
            color: 'teal',
            perc: '75',
        },
        {
            name: 'Bootstrap',
            color: 'purple',
            perc: '60',
        },
        {
            name: 'Tailwind CSS',
            color: 'teal',
            perc: '70',
        },
        {
            name: 'Material-UI',
            color: 'blue',
            perc: '55',
        },
        {
            name: 'Chakra',
            color: 'teal',
            perc: '70',
        },
    ],
];

export const tools = [
    [
        {
            name: 'Git',
            color: 'orange',
            perc: '95',
        },
        {
            name: 'MySQL',
            color: 'blue',
            perc: '70',
        },
        {
            name: 'MongoDB',
            color: 'green',
            perc: '60',
        },
        {
            name: 'Firebase',
            color: 'yellow',
            perc: '40',
        },
    ],
];
